package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"unicode/utf8"
	"strconv"
)

func check(err error) {
	if err != nil {
		panic(err)
	}
}

// get Segmentations from train set
func get_segs(path string) [][]string {
	var sentences []string
	file, err := os.Open(path)
	check(err)
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		sentences = append(sentences, scanner.Text())
	}
	check(scanner.Err())
	//sentences = sentences[:10]
	segs := [][]string{}
	for _, words := range sentences {
		seg := strings.Split(words, " ")
		segs = append(segs, seg)
	}
	return segs
}

func get_dict(train_set string) map[string][]string {
	dic := make(map[string][]string)
	for _, seg := range get_segs(train_set) {
		first_word := string(seg[0])
		if val, ok := dic[first_word]; ok {
			val = append(val, seg...)
			dic[first_word] = val
		} else {
			dic[first_word] = seg
		}
	}
	return dic
}

func BakeOff(sentence, train_set string) []string {
	dic := get_dict(train_set)
	senlen := len(sentence)
	var result []string
	for start, width:= 0,0; start < senlen;{
		runeValue, wd := utf8.DecodeRuneInString(sentence[start:])
		width = wd
		curword := strconv.QuoteRune(runeValue)
		fmt.Println(curword)
		maxlen := width
		if words, ok := dic[curword]; ok {
			for _, word := range words {
				wordlen := len(word)
				if sentence[start:start+wordlen] == word &&
					wordlen > maxlen {
					maxlen = wordlen
				}
			}
		}
		r_word := sentence[start:start+maxlen]
		for i, w:= 0,0; i < len(r_word); i += w {
			runeValue, wd := utf8.DecodeRuneInString(sentence[start:])
			w = wd
			fmt.Println(strconv.QuoteRune(runeValue))
		}
		//fmt.Println(sentence[start:start+maxlen])
		result = append(result, sentence[start:start+maxlen])
		start = start + maxlen
	}
	return result
}

func get_sentences(path string) []string {
	var sentences []string
	file, err := os.Open(path)
	check(err)
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		sentences = append(sentences, scanner.Text())
	}
	check(scanner.Err())
	return sentences
}

func main() {
	train_set := "training.utf8"
	/*for _, seg := range get_segs(train_set) {
		fmt.Println(seg)
	}*/
	sentences := get_sentences("test.utf8")
	sentence := sentences[0]
	BakeOff(sentence ,train_set)
}
