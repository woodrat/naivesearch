# -*- coding: utf-8 -*-
import json


def bakeoff(sentence, dic):
    result = []
    start = 0
    senlen = len(sentence)
    while start < senlen:
        curword = sentence[start]
        maxlen = 1
        if curword in dic:
            words = dic[curword]
            for word in words:
                wordlen = len(word)
                if sentence[start:start+wordlen] == word and wordlen > maxlen:
                    maxlen = wordlen
        result.append(sentence[start:start+maxlen])
        start = start + maxlen
    return result


def get_dic(dic_path):
    with open(dic_path, 'r') as data:
        dic = json.loads(data.read())
    return dic

def main():
    dic_path = "dic"
    with open(dic_path, 'r') as data:
        dic = json.loads(data.read())
        
    
    #for key in dic:
    #    print "key: %s value : %s" % (key,dic[key][0])

    for word in bakeoff(u"2001年新年钟声即将敲响。人类社会前进的航船就要驶入21世纪的新航程。中国人民进入了向现代化建设第三步战略目标迈进的新征程。", dic):
        print(word)
    
if __name__ == "__main__":
    main()







