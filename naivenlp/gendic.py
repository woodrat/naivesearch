# -*- coding: utf-8 -*-
import codecs
import sys
import json


def gen_dic(path):
    with codecs.open(path, 'r', 'utf-8') as f:
        lines = f.readlines()
    
    
    #lines = lines[:1]
    #for i in lines:print i

    lines = [line.split() for line in lines]

    words = []
    for line in lines:
        for word in line:
            words.append(word)
    
    #for word in words:print word 

    dic = {}
    for item in words:
        if item is not None and item[0] in dic:
            value = dic[item[0]]
            value.append(item)
            dic[item[0]] = value
        else:
            dic[item[0]] = [item]
    return dic

def main():
    dic_path = "training.utf8"
    data = json.dumps(gen_dic(dic_path))
    with open('dic', 'w') as dic:
        dic.write(data)

if __name__ == "__main__":
    main()
