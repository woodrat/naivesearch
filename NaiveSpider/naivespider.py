# -*- coding: utf-8 -*-
import os
from BeautifulSoup import BeautifulSoup
from urllib2 import urlopen
from urlparse import urlparse, urljoin
import logging
import time
import sets

logger = logging.Logger('NaiveSpider')
sd = logging.StreamHandler()
logger.addHandler(sd)


class NaiveSpider(object):
    def __init__(self):
        self.alow_url = 'computer.hdu.edu.cn'
        self.start_url = 'http://computer.hdu.edu.cn/'
        self.ingore_url_list = ['#']

    def save_to_folder(self, url, data):
        data_folder = urlparse(url).netloc
        if data_folder not in os.listdir('.'):
            logger.info("data_folder not exist, creating")
            os.mkdir(data_folder)

        url_path = urlparse(url).path
        if url_path == '/':
            url_path = '/index.html'
        path_list = url_path.split('/')
        path = '/'.join(path_list[:-1])

        if path.startswith('/'):
            path = path[1:]

        name = path_list[-1]
        path = os.path.join(data_folder, path)
        logger.info("saving : %s" % os.path.join(path, name))
        if not os.path.exists(path):
            os.makedirs(path)
        
        with open(os.path.join(path, name), 'w') as f:
            f.write(data)

    def add_start_url(self, url):
        if self.start_url not in url:
            return urljoin(self.start_url, url)
        return url

    def is_domain_allowed(self, url):
        domain = urlparse(url)
        if domain.netloc == self.alow_url or url.startswith('/'):
            return True
        else:
            return False
            
    def do_crawl(self, url):
        try:
            resp = urlopen(url)
            if resp.code != 200:
                logger.error("urlopen error:%s" % url)
                return []
            html = resp.read()
            self.save_to_folder(url, html)
            bs = BeautifulSoup(html)
            all_a = bs.findAll("a")
            all_href = [a['href'] for a in all_a]
            allowed_hrefs = filter(self.is_domain_allowed, all_href)
            allowed_hrefs = [self.add_start_url(url) for url in allowed_hrefs]
            return allowed_hrefs
        except:
            #ignore
            return []
        
    def run_forever(self):
        start_time = time.time()
        logger.info("starting at %s" % time.ctime())
        try:
            visited_urls = sets.Set()
            unvisited_urls = sets.Set()
            visited_urls.add(self.start_url)
            unvisited_urls.update(self.do_crawl(self.start_url))
            unvisited_urls = unvisited_urls.difference(visited_urls)
            while(len(unvisited_urls) != 0):
                rurls = sets.Set()
                for url in unvisited_urls:
                    visited_urls.add(url)
                    urls = sets.Set(self.do_crawl(url))
                    rurls.update(urls)
                unvisited_urls.update(urls)
                unvisited_urls = unvisited_urls.difference(visited_urls)
                    
                
                
        except KeyboardInterrupt:
            end_time = time.time()
            logger.info("interrupt at %s" % time.ctime())
            logger.info("ran for %d seconds " % (end_time - start_time))


