# -*- coding: utf-8 -*-
from random import randrange


class NaiveBloomFilter(object):
    def __init__(self, size):
        self.size = size
        self.hash_count = 7
        self.bit_array = [False for i in xrange(size)]

    def put(self, string):
        for seed in xrange(self.hash_count):
            result = hash(string+str(randrange(self.hash_count))) % self.hash_count
            self.bit_array[result] = 1

    def have(self, string):
        for seed in xrange(self.hash_count):
            result = hash(string+str(randrange(self.hash_count))) % self.hash_count
            if self.bit_array[result] == 0:
                return "Not in"
        return "Maybe"

bf = NaiveBloomFilter(10)

bf.put('1')
bf.put('2')
bf.put('1')

print bf.have('1')
print bf.have('2')
print bf.have('3')
