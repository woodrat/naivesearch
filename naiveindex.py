# -*- coding: utf-8 -*-
import logging
import json
import os
import sys
from bs4 import BeautifulSoup


logger = logging.Logger('NaiveSpider')
sd = logging.StreamHandler()
logger.addHandler(sd)


try:
    ABSPATH = os.path.abspath(os.path.realpath(os.path.dirname(__file__)))
    LIBDIR = os.path.abspath(os.path.join(ABSPATH,'naivenlp'))
    sys.path.append(LIBDIR)
    from bakeoff import get_dic
    from bakeoff import bakeoff
except ImportError as error:
    logging.error(error)


def get_pages(data_folder):
    pages = {}

    if  not os.path.isdir(data_folder):
        logger.info("data_folder not exist")
        return {}
    
    #for root, dirs, files in os.walk(data_folder):
    #    for page in files:
    #        with open(os.path.join(data_folder, page)) as html:
    #            html = BeautifulSoup(html, "html.parser")
    #            title = html.title.string
    #            if title not in pages:
    #                pages[title] = os.path.join(data_folder, page)

    for page in os.listdir(data_folder):
        with open(os.path.join(data_folder, page)) as html:
            html = BeautifulSoup(html, "html.parser")
            title = html.title.string
            if title not in pages:
                pages[title] = os.path.join(data_folder, page)

    return pages


inverted_index = {}


def inverted_index_insert(word, title):
    if word not in inverted_index:
        inverted_index[word] = set()
        inverted_index[word].add(title)
    else:
        inverted_index[word].add(title)

def inverted_index_search(words):
    """ sentences have to be bakeoff to words in order to search"""
    # get firest result
    
    
    if words == [] or words is None:
        return None
    
    result = set()
    first_word = words[0]
    if first_word in inverted_index:
        result = inverted_index[first_word]
    
    # get result intersection
    for word in words:
        if word in inverted_index:
            result = result & inverted_index[word]

    return result

dic_path = os.path.join(LIBDIR, "dic")
dic = get_dic(dic_path)
def make_index():
    data_folder = "articles"
    pages = get_pages(data_folder)
    for title in pages:
        words = bakeoff(title, dic)
        for word in words:
            inverted_index_insert(word, title)
    
    index_data = {}
    for item in inverted_index:
        index_data[item] = list(inverted_index[item])
    data = json.dumps(index_data)
    with open('index', 'w') as index:
        index.write(data)

def get_index():
    index_path = os.path.join(ABSPATH, 'index')
    with open(index_path, 'r') as index:
        data = index.read()
    
    global inverted_index
    data = json.loads(data)
    for item in data:
        inverted_index[item] = set(data[item])


def cut(sentence):
    return bakeoff(sentence, dic)

def test():
    get_index()
    words = [u"计算机", u"研究生", u"奖学金"]

    for title in inverted_index_search(words):
        print(title)
        
    
if __name__ == "__main__":
    test()
