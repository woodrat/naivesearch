# -*- coding: utf-8 -*-
from flask import Flask
from flask import request, render_template, jsonify

import os
import sys

BASEDIR = os.path.abspath(os.path.realpath('..'))
LIBDIR = os.path.abspath(os.path.realpath('../naivenlp'))
sys.path.append(BASEDIR)
sys.path.append(LIBDIR)
from naiveindex import get_index, inverted_index_search, cut, get_pages

get_index()

pages = get_pages(os.path.join(BASEDIR, 'articles'))

web = Flask(__name__)
web.config['debug']=True

@web.route('/', methods=['GET','POST'])
def index():
    if request.method == "POST":
        sentence = request.form.get('data')
        print sentence
        words = cut(sentence)
        print words
        results = inverted_index_search(words)
        print results
        paths = {}
        for result in results:
            paths[result] = pages[result]
        
        
        return jsonify(data=paths)
    elif request.method == "GET":
        return render_template('index.html')


if __name__ == '__main__':
    web.run(host='0.0.0.0', port=8080,debug=True)
